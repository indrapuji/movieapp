export { default as Login } from "./Login";
export { default as Main } from "./Main";
export { default as Detail } from "./Detail";
export { default as About } from "./About";
