import React, { Component } from "react";
import { StyleSheet, View, Text, Image, TextInput, TouchableOpacity } from "react-native";

export default class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      password: "",
      isError: false,
    };
  }
  loginHandler() {
    if (this.state.password === "123456") {
      this.props.navigation.push("Main", { userName: this.state.userName });
    } else {
      this.setState({ isError: true });
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <View>
          <Image source={require("../assets/wallpaper.jpeg")} style={styles.imgBackground} />
        </View>
        <View style={{ position: "absolute", left: 76, top: 122 }}>
          <Text style={styles.textTitle}>Movie Source</Text>
          <Text style={styles.textSub}>Online database for movies & television</Text>
        </View>
        <View style={{ position: "absolute", left: 66, top: 382 }}>
          <TextInput
            style={styles.userInput}
            placeholder="username"
            onChangeText={(userName) => this.setState({ userName })}
          />
        </View>
        <View style={{ position: "absolute", left: 66, top: 436 }}>
          <TextInput
            style={styles.userInput}
            placeholder="password"
            onChangeText={(password) => this.setState({ password })}
            secureTextEntry={true}
          />
        </View>
        <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>
          Password Salah
        </Text>
        <View style={{ position: "absolute", left: 66, top: 490 }}>
          <TouchableOpacity style={styles.buttonBorder} onPress={() => this.loginHandler()}>
            <Text style={styles.buttonText}>login</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
    position: "relative",
  },
  userInput: {
    width: 242,
    height: 44,
    backgroundColor: "white",
    borderRadius: 15,
    paddingLeft: 20,
    fontWeight: "bold",
    fontSize: 20,
  },
  imgBackground: {
    width: 373,
    height: 667,
  },
  textTitle: {
    color: "white",
    fontSize: 36,
    fontWeight: "bold",
    lineHeight: 42,
  },
  textSub: {
    color: "white",
    fontSize: 12,
    lineHeight: 14,
  },
  buttonBorder: {
    width: 242,
    height: 44,
    backgroundColor: "blue",
    borderRadius: 25,
  },
  buttonText: {
    textAlign: "center",
    fontSize: 30,
    color: "white",
    fontWeight: "bold",
  },
  errorText: {
    position: "absolute",
    color: "red",
    fontWeight: "bold",
    textAlign: "center",
    marginBottom: 20,
    top: 100,
    left: 66,
  },
  hiddenErrorText: {
    color: "transparent",
    textAlign: "center",
    marginBottom: 20,
  },
});
