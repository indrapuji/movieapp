import React, { useEffect } from "react";
import { StyleSheet, View, Text, TouchableOpacity, ScrollView, FlatList } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { nowPlaying } from "../store/actions/getMovie";
import NowPlayingCard from "../components/NowPlayingCard";

export default (props) => {
  const dispatch = useDispatch();
  function aboutHandler() {
    props.navigation.push("About");
  }

  useEffect(() => {
    dispatch(nowPlaying());
  }, []);
  const files = useSelector((state) => state.playingnow);
  return (
    <View>
      <View style={styles.container}>
        <View style={{ alignItems: "center", flex: 1, justifyContent: "center" }}>
          <TouchableOpacity onPress={() => aboutHandler()}>
            <Text style={{ fontSize: 20 }}>Hello {props.route.params.userName}</Text>
          </TouchableOpacity>
        </View>
      </View>
      <Text style={styles.titleText}>Popular Movie</Text>
      <FlatList
        data={files}
        renderItem={({ item, index }) => <NowPlayingCard list={item} />}
        keyExtractor={(key, index) => index.toString()}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  titleText: {
    fontSize: 25,
    fontWeight: "bold",
    textAlign: "center",
    marginVertical: 15,
  },
  titleTextMore: {
    fontSize: 18,
    fontWeight: "bold",
    color: "blue",
  },
  container: {
    backgroundColor: "blue",
    height: 50,
  },
});
