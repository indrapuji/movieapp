export function nowPlaying() {
  return (dispatch, getState) => {
    fetch(
      "https://api.themoviedb.org/3/movie/popular?api_key=464b6412840269fe91e87ba7d6958784&language=en-US&page=1"
    )
      .then((res) => res.json())
      .then((data) => {
        dispatch({
          type: "NOWPLAYING",
          payload: data.results,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
}
