const initialState = {
  playingnow: [],
};

function reducer(state = initialState, action) {
  if (action.type === "NOWPLAYING") {
    return { ...state, playingnow: action.payload };
  }
  return state;
}

export default reducer;
