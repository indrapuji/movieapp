import React from "react";
import { Login, Main, Detail, About } from "./screens";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import "react-native-gesture-handler";
import { Provider } from "react-redux";
import store from "./store";

const Stack = createStackNavigator();
export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
          <Stack.Screen name="Main" component={Main} options={{ headerTitle: false }} />
          <Stack.Screen name="About" component={About} options={{ headerTitle: false }} />
        </Stack.Navigator>
      </NavigationContainer>
      {/* <Login /> */}
      {/* <Main /> */}
      {/* <Detail /> */}
    </Provider>
  );
}
