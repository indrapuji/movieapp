import React from "react";
import { StyleSheet, View, Text, TouchableOpacity, Image } from "react-native";

export default function NowPlayingCard(props) {
  return (
    <View>
      <TouchableOpacity>
        <View style={styles.container}>
          <Image
            source={{ uri: `https://image.tmdb.org/t/p/w500/${props.list.backdrop_path}` }}
            style={{ width: 300, height: 150 }}
          />
          <View style={{ width: 300, marginTop: 5 }}>
            <Text numberOfLines={2} style={{ marginBottom: 5, textAlign: "center" }}>
              {props.list.title}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    // width: 300,
  },
});
