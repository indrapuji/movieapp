import React from "react";
import { StyleSheet, View, Text } from "react-native";

export default function Header(props) {
  return (
    <View style={styles.container}>
      <View style={{ alignItems: "center", flex: 1, justifyContent: "center" }}>
        <Text style={{ fontSize: 20 }}>Hello {props.route.params.userName}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "blue",
    height: 50,
  },
});
